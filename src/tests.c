#include "tests.h"
#include <unistd.h>

static void print_and_debug_heap(const void* const heap, const char* const msg) {
    printf("%s: %s", msg, "\n");
    debug_heap(stdout, heap);
}

bool test_usual_success_alloc() {
    size_t heap_size = 4096;
    void* test_heap = heap_init(heap_size);
    if (!test_heap) return false;
    print_and_debug_heap(test_heap, "Init");
    void* allocate_memory = _malloc(heap_size / 2);
    if (!allocate_memory) return false;
    print_and_debug_heap(test_heap, "Alloc");
    _free(allocate_memory);
    print_and_debug_heap(test_heap, "Free");

    munmap(test_heap, size_from_capacity((block_capacity) {.bytes = heap_size}).bytes);
    
    return true;
}

bool test_single_block_free() {
    size_t heap_size = 4096;
    void* test_heap = heap_init(heap_size);
    if (!test_heap) return false;
    print_and_debug_heap(test_heap, "Init");
    void* allocate_memory1 = _malloc(1024);
    print_and_debug_heap(test_heap, "Init first block");
    void* allocate_memory2 = _malloc(1024);
    print_and_debug_heap(test_heap, "Init second block");
    if (!allocate_memory1 || !allocate_memory2) return false;
    print_and_debug_heap(test_heap, "Alloc");
    _free(allocate_memory1);
    print_and_debug_heap(test_heap, "Free first block");
    if (!allocate_memory2) return false;
    _free(allocate_memory2);
    print_and_debug_heap(test_heap, "Free second block");

    munmap(test_heap, size_from_capacity((block_capacity) {.bytes = heap_size}).bytes);

    return true;
}

bool test_double_block_free() {
    size_t heap_size = 4096;
    void* test_heap = heap_init(heap_size);
    if (!test_heap) return false;
    print_and_debug_heap(test_heap, "Init");
    void* allocate_memory1 = _malloc(1024);
    void* allocate_memory2 = _malloc(1024);
    void* allocate_memory3 = _malloc(1024);
    if (!allocate_memory1) return false;
    if (!allocate_memory2 || !allocate_memory3) return false;
    if (!allocate_memory3) return false;
    print_and_debug_heap(test_heap, "Alloc");
    _free(allocate_memory1);
    _free(allocate_memory3);
    if (!allocate_memory2) return false;
    print_and_debug_heap(test_heap, "Free");

    munmap(test_heap, size_from_capacity((block_capacity) {.bytes = heap_size}).bytes);
    
    return true;
}

bool test_grow_heap_and_merge() {
    size_t heap_size = 4096;
    void* test_heap = heap_init(heap_size);
    print_and_debug_heap(test_heap, "Init");
    if (!test_heap) return false;
    void* first_alloc = _malloc(heap_size * 2);
    print_and_debug_heap(test_heap, "Alloc");
    if (!first_alloc) {
        munmap(test_heap, size_from_capacity((block_capacity) {.bytes = heap_size}).bytes);
        return false;
    }
    struct block_header* heap_header = (struct block_header*) test_heap;
    if (heap_header->capacity.bytes < heap_size * 2) {
        munmap(test_heap, size_from_capacity((block_capacity) {.bytes = heap_size}).bytes);
        return false;
    }
    _free(first_alloc);
    print_and_debug_heap(test_heap, "Free");

    munmap(test_heap, size_from_capacity((block_capacity) {.bytes = heap_size}).bytes);
    return true;
}

bool test_grow_heap_no_merge() {
    size_t heap_size = 4096;
    void* test_heap = heap_init(heap_size);
    print_and_debug_heap(test_heap, "Init");
    if (!test_heap) return false;
    size_t wall_size = 1024;
    void* wall = mmap(test_heap + heap_size, wall_size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    size_t alloc_size = heap_size * 2;
    void* first_alloc = _malloc(alloc_size);
    print_and_debug_heap(test_heap, "Alloc");
    if (!first_alloc) {
        munmap(wall, wall_size);
        munmap(test_heap, size_from_capacity((block_capacity) {.bytes = heap_size}).bytes);
        return false;
    }
    struct block_header* heap_header = (struct block_header*) test_heap;
    if (!heap_header->is_free || heap_header->next->is_free) {
        munmap(wall, wall_size);
        munmap(test_heap, size_from_capacity(heap_header->capacity).bytes);
        munmap(first_alloc, alloc_size);
        return false;
    }
    _free(first_alloc);
    print_and_debug_heap(test_heap, "Free");

    munmap(wall, wall_size);
    munmap(test_heap, size_from_capacity(heap_header->capacity).bytes);
    munmap(first_alloc, alloc_size);
    return true;
}

test_func simple_test_funcs[] = {
        test_usual_success_alloc,
        test_single_block_free,
        test_double_block_free,
        test_grow_heap_and_merge,
        test_grow_heap_no_merge
};

void test_handler(test_func test, size_t num) {
    fprintf(stdout, "\n---------------- TEST %zu ----------------\n", num);
    if (!test()) fprintf(stdout, "TEST %zu failed\n", num);
    else fprintf(stdout, "TEST %zu passed\n", num);
}

size_t simple_test_funcs_count = 5;

void execute_tests(test_func* tests, size_t tests_count, test_func_handler handler) {
    for (size_t i = 0; i < tests_count; i++) {
        handler(tests[i], i+1);
    }
}
