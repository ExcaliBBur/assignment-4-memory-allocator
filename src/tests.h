#ifndef TESTS_H_
#define TESTS_H_

#include "mem.h"
#include "mem_internals.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>

typedef bool (* test_func)(void);

extern test_func simple_test_funcs[];

extern size_t simple_test_funcs_count;

typedef void (* test_func_handler)(test_func test, size_t num);

void test_handler(test_func test, size_t num);

void execute_tests(test_func* tests, size_t tests_count, test_func_handler handler);

#endif
